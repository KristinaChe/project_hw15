#include <iostream>

void getEvenOrOdd(int a, int N)
{
    for (int x = 0; x <= N; x++)
    {
        if (x % 2 == a)
            std::cout << x << "\n";
    }
}
int main()
{
    static int N;
    int a;

    std::cout << "Enter a positive number: ";
    std::cin >> N;
    while (N < 0)
    {
        std::cout << "Enter a POSITIVE number: ";
        std::cin >> N;
    }

    std::cout << "Enter '0' for output even numbers or '1' - for odd: ";
    std::cin >> a;
    while (a < 0 || a > 1)
    {
        std::cout << "'0' or '1': ";
        std::cin >> a;
    }
  
    getEvenOrOdd(a, N);

    return 0;
}
